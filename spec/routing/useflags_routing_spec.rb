require "rails_helper"

RSpec.describe UseflagsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/useflags").to route_to("useflags#index")
    end

    it "routes to #new" do
      expect(:get => "/useflags/new").to route_to("useflags#new")
    end

    it "routes to #show" do
      expect(:get => "/useflags/1").to route_to("useflags#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/useflags/1/edit").to route_to("useflags#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/useflags").to route_to("useflags#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/useflags/1").to route_to("useflags#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/useflags/1").to route_to("useflags#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/useflags/1").to route_to("useflags#destroy", :id => "1")
    end
  end
end
