require "rails_helper"

RSpec.describe ChangesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/changes").to route_to("changes#index")
    end

    it "routes to #new" do
      expect(:get => "/changes/new").to route_to("changes#new")
    end

    it "routes to #show" do
      expect(:get => "/changes/1").to route_to("changes#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/changes/1/edit").to route_to("changes#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/changes").to route_to("changes#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/changes/1").to route_to("changes#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/changes/1").to route_to("changes#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/changes/1").to route_to("changes#destroy", :id => "1")
    end
  end
end
