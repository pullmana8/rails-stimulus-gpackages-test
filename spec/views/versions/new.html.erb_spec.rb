require 'rails_helper'

RSpec.describe "versions/new", type: :view do
  before(:each) do
    assign(:version, Version.new())
  end

  it "renders new version form" do
    render

    assert_select "form[action=?][method=?]", versions_path, "post" do
    end
  end
end
