require 'rails_helper'

RSpec.describe "useflags/edit", type: :view do
  before(:each) do
    @useflag = assign(:useflag, Useflag.create!())
  end

  it "renders the edit useflag form" do
    render

    assert_select "form[action=?][method=?]", useflag_path(@useflag), "post" do
    end
  end
end
