class Category
  include ActiveModel::Model # supplies some methods necessary for form helpers
  include ActiveModel::Validations # provides error message caching and validations methods

  ATTRIBUTES = [:name,
                :description,
                :metadata_hash]
  attr_accessor(*ATTRIBUTES)
  attr_reader :attributes
  # attr_reader :category, :name, :path

  def initialize(attr={})
    attr.each do |k,v|
      if ATTRIBUTES.include?(k.to_sym)
        send("#{k}=", v)
      end
    end
  end

  def attributes
    ATTRIBUTES.inject({}) do |hash, attr|
      if value = send(attr)
        hash[attr] = value
      end
      hash
    end
  end
  alias :to_hash :attributes
end
