class CategorySerializer < JSONAPI::Serializable::Resource
  type 'categories'

  attributes :id,
    :name,
    :description,
    :metadata_hash
end
